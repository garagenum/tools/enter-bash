#!/bin/bash

cat <<- EOF1 >> /home/level4/.bashrc
cat << NestedEOF1       


        Vous êtes au 4eme niveau.

        Certains fichiers sont trop longs être lus avec cat.

        Nous allons utiliser la commande "less" pour naviguer dans le fichier.

        Une fois dans "less", utilisez les flèches haut et bas pour naviquer,

        et appuyez sur q pour quitter.

        Vous trouverez le mot de passe au milieu du fichier.

        Saisissez la commande suivante:

                less README

        Quand vous aurez trouvé le mot de passe, connectez-vous au niveau 5:

                su - level5

NestedEOF1
EOF1

touch /home/level4/README

for i in {1..90} ; do shuf -n4 /usr/share/dict/words |tr '\n' ' ' >> /home/level4/README \
&& echo ' ' >> /home/level4/README ; done 
echo "mot de passe : 830-bus-fux" >> /home/level4/README 
for i in {1..90} ; do shuf -n4 /usr/share/dict/words |tr '\n' ' '  >> /home/level4/README \
&& echo ' ' >> /home/level4/README ; done


