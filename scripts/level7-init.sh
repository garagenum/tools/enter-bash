echo "echo 'Lisez le README avec la commande less README' \(q pour quitter\)" >>  /home/level7/.bashrc

touch /home/level7/README

cat << EOF1 > /home/level7/README

La différence entre un script et un logiciel classique, c'est qu'un script,
ça tient dans un seul fichier, dont on peut lire le contenu.

Pour lire le contenu du script, c'est pareil que pour lire le contenu du
README, mais en remplaçant README dans la commande par le nom du script.

Pour accéder, au niveau 8, lisez le contenu du script nommé script.sh

EOF1



touch /home/level7/script.sh

cat << 'EOF2' > /home/level7/script.sh
#!/bin/bash

# Ceci est un script
# Les lignes commençant par '#' ne sont pas 'interprétées', c'est-à-dire
# que les commandes qu'elles contiennent ne sont pas exécutées.
# Cela permet d'écrire des commentaires ou des explications dans le script.

echo " Le mot de passe du niveau 8 est fuf-612-vhi"


EOF2
