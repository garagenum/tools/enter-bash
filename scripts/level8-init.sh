echo "echo 'Devenez Technicien DevOps'" >>  /home/level8/.bashrc

touch /home/level8/README

cat <<EOF1 > /home/level8/README

Le Technicien DevOps c'est quelqu'un qui peut utiliser et écrire des scripts 

pour faciliter la maintenance des ordinateurs et des serveurs.


Commencez dès maintenant à "débuguer", c'est à dire à trouver le problème 

dans le code-source du script (le contenu du fichier) !



1/ Essayez d'exécuter le script nommé script.sh à l'aide de la commande:

    bash script.sh 

Vous verrez qu'il y a une erreur qui s'affiche, alors que le script devrait
nous afficher le mot de passe pour le niveau 9...



2/ Lisez alors le contenu du script (son code-source) avec less:

    less script.sh



3/ Quand vous aurez trouvé l'erreur, exécutez un autre script, nommé debug.sh:

    bash debug.sh


Avec ça, vous devriez pouvoir passer au niveau 9 !


EOF1


touch /home/level8/script.sh
touch /home/level8/.clear.sh
cat <<- 'EOF2' > /home/level8/.clear.sh


	SECRET2=$(echo `cat /dev/urandom |tr -dc [:alnum:] |fold -w 3 |head -n10` wqp `cat /dev/urandom |tr -dc [:alnum:] |fold -w 3 |head -n10` 230 `cat /dev/urandom |tr -dc [:alnum:] |fold -w 3 |head -n10` ang)
	cat << 'EOF3' > /home/level8/script.sh
	Voici un script permettant d\'afficher le mot de passe du niveau 9 &&
	. .clear.sh   && echo $SECRET2 |sed -e "s/ /-/g" |cut -d-  -f11,22,33  &&   bash ./.clear.sh && exit
EOF3

EOF2

bash /home/level8/.clear.sh


touch /home/level8/debug.sh
cat << 'EOF4' > /home/level8/debug.sh

  cat <<- 'EOF5'
	
	Quel est le caractère à ajouter au début de la 1ère ligne du script 
	
	pour qu'il fonctionne ?

    Répondez et validez avec Entrée:
    	
EOF5
	

PASS="

Féliciations ! 
	Vous avez corrigés le script ! 

    Exécutez le à nouveau avec la commande: 
    
        bash script.sh 

"

TRY="

	Ce n'est pas ça !

	Lisez à nouveau le fichier pour trouver l'erreur :

		less script.sh

	puis exécutez à nouveau ce script:

		bash debug.sh

"

read CHAR

[[ $CHAR == "#" ]] &&  sed "1s/^/# /" /home/level8/script.sh > /tmp/script.sh && cat /tmp/script.sh > /home/level8/script.sh && rm -f /tmp/sript.sh && echo "${PASS}" || echo "${TRY}"

EOF4

