FROM debian:bullseye-slim
RUN apt update -y
RUN apt install tree less openssh-server wamerican accountsservice -y
RUN useradd -rm -d /home/debian -s /bin/bash -g root -G sudo -u 1000 test
RUN service ssh start
COPY . /home/debian
EXPOSE 22
RUN /home/debian/deploy.sh
CMD ["/usr/sbin/sshd", "-D"]