#!/bin/bash

cat <<- EOF1 >> /home/level3/.bashrc
cat << NestedEOF1	
	
	
	Bravo, vous êtes déjà arrivés au 3eme niveau!
	
	Nous allons maintenant apprendre à regarder le contenu d'un fichier.
	
	Avec la commande ls, vous voyer un fichier nommé "README" (Lisez-moi).
	
	Saisissez la commande suivante pour afficher son contenu:
	
		cat README
	
	Écrivez bien README en majuscules
	
	Connectez-vous maintenant au niveau 4 avec la commande
	
	su - level4
	
NestedEOF1
EOF1

touch /home/level3/README
echo "mot de passe : bvi-rsk-820" > /home/level3/README

