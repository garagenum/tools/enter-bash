# ENTER-BASH

Bash-enter is a game to discover Bash.
You have to go through different levels, using ssh.
For each level, you have to find the password for the next level.
Each level's user is named level1, level2, level3, etc.

## DEPLOIEMENT

- Build the image:
```bash
cd enter-bash
docker build -t enter-bash:1.0 .
```

- Start the container:
```bash
docker run -d -p 5555:22 enter-bash:1.0
```

Note: The port 5555 is the docker's host port mapped to the ssh port of the container (22), you can choose a port to your liking.


