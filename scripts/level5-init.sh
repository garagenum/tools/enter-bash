#!/bin/bash

cat <<- EOF1 >> /home/level5/.bashrc
cat << NestedEOF1	
	
	
	Vous êtes au 5eme niveau.
	
	C'est presque la dernière fois que vous verrez un message d'accueil.
	
	Il vous faudra désormais lire le contenu du fichier README.
	
	Saisissez la commande 
	
		less README
	
	et suivez les indications.
	
	Appuyez sur la touche q pour quitter less !
	
	
NestedEOF1
EOF1

touch /home/level5/README

cat << EOF2 > /home/level5/README

Pour trouver le mot de passe du 5ème niveau, 

vous allez exécuter un script.

Un script, c'est comme un petit logiciel, qui tient dans un seul fichier.

Pour exécuter un script, on utilise la commande bash, 
suivi du nom du fichier.

Saisissez la commande

	bash script.sh
	
et appuyez sur Entrée. Connectez-vous ensuite au niveau 6 avec 

	su - level6

EOF2

touch /home/level5/script.sh
chmod u+x /home/level5/script.sh

cat << EOF3 > /home/level5/script.sh
#!/bin/bash

echo " le mot de passe est wdo-810-289-bou-104-910-429-fun " |sed  's/[[:digit:]]\+\-//g'

EOF3


