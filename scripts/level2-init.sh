#!/bin/bash

cat <<- EOF1 >> /home/level2/.bashrc
cat << NestedEOF1	
	
	Super!


Bienvenue dans le niveau 2 !

Vous êtes en train d'utiliser Linux, un SYSTÈME D'EXPLOITATION OPEN-SOURCE.

Nous allons apprendre quelques commandes de base pour manipuler le système.

Commencez par écrire la commande "ls" (ne mettez pas les guillemets)

Appuyez ensuite sur Entrée

Vous voyez la liste des fichiers du DOSSIER COURANT

Le nom de l'un des fichiers est le mot de passe pour le niveau 3.

Tapez su - level3 puis Entrée et saisissez le mot de passe



NestedEOF1
EOF1


for i in {1..6} ; do
  touch /home/level2/fichier$i ;
  cat <<- EOF2 >> /home/level2/fichier$i
  
  Ceci est le contenu du fichier nommé fichier$i
  
EOF2
done

for i in {1..3} ; do
  mkdir /home/level2/dossier$i ; 
done

touch /home/level2/ihj-pli-444
