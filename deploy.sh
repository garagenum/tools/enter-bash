#!/bin/bash

# Execute the script as root


### add some packages
apt -y update
apt -y install tree wamerican

### Création des utilisateurs
  
  touch /etc/skel/.hushlogin # To suppress /etc/motd and Gandi message

  for i in {1..10} ; do deluser level$i ; delgroup level$i ; rm -Rfv /home/level$i ; done
  
  # hashes are done with mkpassword -m shA-512 PASSWORD . First column ($ as sep.) is for method, 2nd is for seed, 3 is encrypted pwd.
  
  useradd -m -s /bin/bash -p '$6$12345678$z6Yi8kgUombQ0LWF6tYGnRpKjqMgm.NULCC03xD6mDMclpCsfSZfVR5ehRTdEgb0i09PVLN51cUTJAyjWZTr41' level1 #knj-yhn-576
  useradd -m -s /bin/bash -p '$6$12345678$QLwlPOpn3lOH988Xamb1GaSSkG5JSIWcrkx.b0sFhPU9ceSpPOSLeE265PX23zXEWF3dz/nbJEDMpIRMW7OB0.' level2 #kbc-209-xdf
  useradd -m -s /bin/bash -p '$6$12345678$h4IhZhhMPr/h6gVyWVZwTYFldW/clQwTDdheLhiTKmrgj.vv4KSBTYaOEchzfz73X0hpFjRcE6bDc6l8FXZfW1' level3 #ihj-pli-444
  useradd -m -s /bin/bash -p '$6$eG7L5FVdvn$WnxWH5Cjt.mOUElddKez66cW4SAOQGwslbChK5EiQgac9rDln/c1VYjXjSzY9XrkmeWQCTjgWOtQ4oD9Qly9s/' level4 #bvi-rsk-820
  useradd -m -s /bin/bash -p '$6$qA//d3/4X$KNnEkQiGHzJcahwZdKs6PBgTB7NHDj6QKMWMcErD0qkbltAV6/8QuSym6yGTUs8b3ksPlMHrX2kdWIrUqbidL1' level5 #830-bus-fux
  useradd -m -s /bin/bash -p '$6$e1.zNiSsuIB$xYF7oMVVcHoQDLljdcSGN4g/Gj3/TxU1mdHv5LX5gtplOA5RBOMh1Jr.JFcGNPbg1SDjUCit7jpF//2L0m1AV.' level6 #wdo-bou-fun
  useradd -m -s /bin/bash -p '$6$P6Olk2sRKl$JUJlERtQxYivrW.igFcg7N0K2p83Ed1RyYws..LrblMLQYopxGApPnjTyAYxOjr75l5O73hFBNie0BneBBvWp/' level7 #ocj-eyf-thc
  useradd -m -s /bin/bash -p '$6$CrgxMXpbZDTE6Ta6$iYtpCcmRk9Bo.KSCSzL.99B309ZhD5qgvNr8K3jYz5ugiLknKP6m.hORlfJx2QUfsASeSj4m25vlDX3c4unBV1' level8 #fuf-612-vhi
  useradd -m -s /bin/bash -p '$6$2lgI6wKPuF98gNW$fG8kCkaQkxxTEsg/PKnMCdqXblN9UHVSr6xESJlmHA9luUqFm4.zPLCFHDx6IwgTSo95Z9CKLY09q19lkrYNn/' level9 #wqp-230-ang
  useradd -m -s /bin/bash -p '$6$qRV0jKJxhWmb$kxs4fKMkLg2diqNdAqPpA8lZ4cW5OoR8Rf7xBnJp1L2btsKDKFpIAA9manXUMsXplmGoEqwjGcBH0QG4ws7nA.' level10 #qpe-fvx-718


for i in {1..10}; do 
	bash scripts/level$i-init.sh
done

for i in {1..10}; do chown -Rfv level$i:level$i /home/level$i  ; chmod ug+x /home/level$i/.*.sh ; chmod ug+x /home/level$i/*.sh ; chmod -R a-w /home/level$i ; chmod ug+w /home/level8/script.sh; done

touch /var/lib/AccountsService/users/level{1..10}
for i in /var/lib/AccountsService/users/level{1..10} ; do
	cat << EOF >> $i
[User]
Language=
XSession=gnome
SystemAccount=true

EOF
done
