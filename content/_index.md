Pour découvrir bash ,

démarrez un terminal et connectez-vous avec la commande suivante:  
**ssh -p 4444 level1@bash.legaragenumerique.fr** 

```
visiteur@mypc:~$ ssh -p 4444 level1@bash.legaragenumerique.fr   
level1@bash.legaragenumerique.fr's password: 
```

Écrivez le mot de passe: **knj-yhn-576**.

Rien ne s'affiche, c'est normal, c'est pour que la personne qui regarde derrière votre épaule ne connaisse pas le nombre de caractères du mot de passe.

Appuyez ensuite sur Entrée pour valider le mot de passe.



