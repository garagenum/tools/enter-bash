echo "echo 'Lisez le README avec la commande less README (q pour quitter) ' " >>  /home/level6/.bashrc

touch /home/level6/README

cat << EOF2 > /home/level6/README

Les scripts peuvent aussi être interactifs, et demander à l'utilisateur
de confirmer une action ou d'écrire une information.

Éxecutez le script présent dans le dossier avec la commande:

bash script.sh

EOF2

touch /home/level6/script.sh
chmod u+x /home/level6/script.sh

cat << 'EOF3' > /home/level6/script.sh
#!/bin/bash

  cat <<- 'EOF4'
    Pour obtenir le mot de passe, 
    
    Répondez à la question suivante et validez avec Entrée:
    
    Quel est le système d'exploitation que nous utilisons actuellement?
	
	EOF4

read os_used
os_low="$(echo $os_used |tr [:upper:] [:lower:])"
[[ "$os_low" = "linux" ]] \
&& printf "\nC'est bien ça ! \n\nle mot de passe du niveau 7 est ocj-728-289-eyf-267-108-thc \n\nConnectez-vous avec le commande \n\n su - level7 \n" |sed  's/[[:digit:]]\+\-//g' \
|| printf "Non, ce n\'est pas $os_low, relancez le script pour essayer à nouveau\n"


EOF3
