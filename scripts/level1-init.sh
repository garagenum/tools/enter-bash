#!/bin/bash
cat <<- EOF1 >> /home/level1/.bashrc
cat << NestedEOF1	
	
Bienvenue!

Vous avez réussi à vous connecter au serveur!

Pour passer au niveau 2, il faut écrire la commande suivante:

      su - level2

La commande su permet de changer d'utilisateur (Switch User).
N'oubliez pas le "-" (tiret du 6) entre "su" et "level2" 

Appuyez ensuite sur Entrée, et saisissez le mot de passe suivant:

      kbc-209-xdf


puis appuyez sur Entrée à nouveau!


NestedEOF1
EOF1




